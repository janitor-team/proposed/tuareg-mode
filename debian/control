Source: tuareg-mode
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Ralf Treinen <treinen@debian.org>,
 Samuel Mimram <smimram@debian.org>,
 Stéphane Glondu <glondu@debian.org>
Build-Depends: debhelper-compat (= 12),
 dh-elpa
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/ocaml-team/tuareg-mode
Vcs-Git: https://salsa.debian.org/ocaml-team/tuareg-mode.git
Homepage: https://github.com/ocaml/tuareg
Testsuite: autopkgtest-pkg-elpa

Package: elpa-tuareg
Architecture: all
Depends: ${elpa:Depends},
 ${misc:Depends}
Recommends: emacs (>= 46.0),
 ocaml-interp,
 elpa-caml
Enhances: ocaml-interp,
 emacs
Breaks: tuareg-mode (<< 1:2.1.0-3)
Replaces: tuareg-mode (<< 1:2.1.0-3)
Description: emacs-mode for OCaml programs
 Tuareg handles automatic indentation of Objective Caml and Caml-Light
 code.  Key parts of the code are highlighted using Font-Lock. It
 provides support to run an interactive OCaml toplevel and debugger.
 .
 This mode attempts to give better results than the caml-mode provided by
 the elpa-caml package. Indentation rules are slightly different but
 closer to classical functional languages indentation. Tuareg
 gives access to some functionalities from caml-mode when the elpa-caml
 package is installed.

Package: tuareg-mode
Architecture: all
Depends: elpa-tuareg,
 ${misc:Depends}
Section: oldlibs
Description: transitional package, tuareg-mode to elpa-tuareg
 Tuareg for GNU Emacs has been elpafied.  This dummy transitional
 package facilitates the transition from tuareg-mode to
 elpa-tuareg-mode and is safe to remove.
